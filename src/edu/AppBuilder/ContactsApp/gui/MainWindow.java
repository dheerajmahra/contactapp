package edu.AppBuilder.ContactsApp.gui;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import edu.AppBuilder.ContactsApp.ContactDetails;
import edu.AppBuilder.ContactsApp.io.IInputOutput;
import javax.swing.JPanel;
import java.awt.Component;
import java.awt.FlowLayout;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class MainWindow {

	private JFrame frame;
	private ContactDetails c;
	private IInputOutput io;
	private JTabbedPane tabbedPane;
	private JPanel panel;
	private JLabel label;
	private JTextField textField;
	private JLabel label_1;
	private JTextField textField_1;
	private JLabel label_2;
	private JTextField textField_2;
	private JLabel label_3;
	private JButton button;
	private JLabel label_4;
	private JLabel label_5;
	private JLabel label_6;
	private JLabel label_7;
	private JPanel panel_1;
	private JScrollPane scrollPane;
	private JPanel panel_2;
	private JTextPane textPane;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// MainWindow window = new MainWindow();
	// window.frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the application.
	 */
	public MainWindow(IInputOutput val) {
		io = val;
		initialize();
		frame.setVisible(true);
		try {
			io.createConnection("File.ContactDetails");
		} catch (Exception e) {

			lblNewLabel.setForeground(Color.RED);
			lblNewLabel.setText(e.getMessage());

		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Contact Application");
		frame.setMinimumSize(new Dimension(400, 300));
		frame.getContentPane().setMinimumSize(new Dimension(450, 300));
		frame.getContentPane().setForeground(Color.RED);
		frame.getContentPane().setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
		frame.setBounds(100, 100, 674, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {

				if (tabbedPane.getSelectedIndex() == 1) {
					try {

						List<String> list = io.printAll();
						for (String s : list) {

							textPane.setText(textPane.getText() + s + "\n");
						}

					}

					catch (Exception e) {

						label.setForeground(Color.RED);
						label.setText(e.getMessage());
					}
				}

			}
		});
		frame.getContentPane().add(tabbedPane);
		
				panel = new JPanel();
				tabbedPane.addTab("Add contact", null, panel, null);
				GridBagLayout gbl_panel = new GridBagLayout();
				gbl_panel.columnWidths = new int[]{129, 86, 1, 1, 81, 1, 1, 71, 86, 137, 1, 0};
				gbl_panel.rowHeights = new int[]{25, 0, 25, 0, 0, 0};
				gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
				panel.setLayout(gbl_panel);
				
						label = new JLabel("Enter name: ");
						label.setForeground(Color.BLACK);
						label.setFont(new Font("Tahoma", Font.BOLD, 20));
						GridBagConstraints gbc_label = new GridBagConstraints();
						gbc_label.anchor = GridBagConstraints.NORTHWEST;
						gbc_label.insets = new Insets(0, 0, 5, 5);
						gbc_label.gridx = 0;
						gbc_label.gridy = 0;
						panel.add(label, gbc_label);
								
										label_4 = new JLabel("");
										GridBagConstraints gbc_label_4 = new GridBagConstraints();
										gbc_label_4.anchor = GridBagConstraints.WEST;
										gbc_label_4.insets = new Insets(0, 0, 5, 5);
										gbc_label_4.gridx = 2;
										gbc_label_4.gridy = 0;
										panel.add(label_4, gbc_label_4);
										
												label_5 = new JLabel("");
												GridBagConstraints gbc_label_5 = new GridBagConstraints();
												gbc_label_5.anchor = GridBagConstraints.WEST;
												gbc_label_5.insets = new Insets(0, 0, 5, 5);
												gbc_label_5.gridx = 3;
												gbc_label_5.gridy = 0;
												panel.add(label_5, gbc_label_5);
														
																textField = new JTextField();
																textField.setColumns(10);
																GridBagConstraints gbc_textField = new GridBagConstraints();
																gbc_textField.fill = GridBagConstraints.HORIZONTAL;
																gbc_textField.gridwidth = 2;
																gbc_textField.insets = new Insets(0, 0, 5, 5);
																gbc_textField.gridx = 4;
																gbc_textField.gridy = 0;
																panel.add(textField, gbc_textField);
												
														label_6 = new JLabel("");
														GridBagConstraints gbc_label_6 = new GridBagConstraints();
														gbc_label_6.anchor = GridBagConstraints.WEST;
														gbc_label_6.insets = new Insets(0, 0, 5, 0);
														gbc_label_6.gridx = 10;
														gbc_label_6.gridy = 0;
														panel.add(label_6, gbc_label_6);
														
																label_1 = new JLabel("Enter phone no: ");
																label_1.setFont(new Font("Tahoma", Font.BOLD, 20));
																GridBagConstraints gbc_label_1 = new GridBagConstraints();
																gbc_label_1.anchor = GridBagConstraints.NORTHWEST;
																gbc_label_1.insets = new Insets(0, 0, 5, 5);
																gbc_label_1.gridwidth = 4;
																gbc_label_1.gridx = 0;
																gbc_label_1.gridy = 1;
																panel.add(label_1, gbc_label_1);
																
																		textField_2 = new JTextField();
																		textField_2.setColumns(10);
																		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
																		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
																		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
																		gbc_textField_2.gridwidth = 2;
																		gbc_textField_2.gridx = 4;
																		gbc_textField_2.gridy = 1;
																		panel.add(textField_2, gbc_textField_2);
																		
																				label_2 = new JLabel("Enter e-mail: ");
																				label_2.setForeground(Color.BLACK);
																				label_2.setFont(new Font("Tahoma", Font.BOLD, 20));
																				GridBagConstraints gbc_label_2 = new GridBagConstraints();
																				gbc_label_2.anchor = GridBagConstraints.NORTHWEST;
																				gbc_label_2.insets = new Insets(0, 0, 5, 5);
																				gbc_label_2.gridx = 0;
																				gbc_label_2.gridy = 2;
																				panel.add(label_2, gbc_label_2);
																								
																										textField_1 = new JTextField();
																										textField_1.setColumns(10);
																										GridBagConstraints gbc_textField_1 = new GridBagConstraints();
																										gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
																										gbc_textField_1.insets = new Insets(0, 0, 5, 5);
																										gbc_textField_1.gridx = 4;
																										gbc_textField_1.gridy = 2;
																										panel.add(textField_1, gbc_textField_1);
																						
																								label_3 = new JLabel("");
																								GridBagConstraints gbc_label_3 = new GridBagConstraints();
																								gbc_label_3.anchor = GridBagConstraints.WEST;
																								gbc_label_3.insets = new Insets(0, 0, 5, 5);
																								gbc_label_3.gridx = 5;
																								gbc_label_3.gridy = 2;
																								panel.add(label_3, gbc_label_3);
																								
																										label_7 = new JLabel("");
																										GridBagConstraints gbc_label_7 = new GridBagConstraints();
																										gbc_label_7.anchor = GridBagConstraints.WEST;
																										gbc_label_7.insets = new Insets(0, 0, 5, 5);
																										gbc_label_7.gridx = 6;
																										gbc_label_7.gridy = 2;
																										panel.add(label_7, gbc_label_7);
																										
																												button = new JButton("SUBMIT");
																												button.addActionListener(new ActionListener() {
																													public void actionPerformed(ActionEvent arg0) {
																														c=new ContactDetails();
																														c.setName(textField.getText().trim());
																														c.setEmail(textField_1.getText().trim());
																														try{
																															
																															c.setPhoneNo(Long.parseLong(textField_2.getText().trim()));
																										
																															io.save(c);
																														} catch(Exception e){
																															
																															label_7.setForeground(Color.RED);
																															label_7.setText(e.getMessage());
																														}
																														
																														
																														
																													}
																												});
																												button.setFont(new Font("Malgun Gothic", Font.BOLD, 12));
																												GridBagConstraints gbc_button = new GridBagConstraints();
																												gbc_button.anchor = GridBagConstraints.NORTHWEST;
																												gbc_button.insets = new Insets(0, 0, 5, 5);
																												gbc_button.gridwidth = 2;
																												gbc_button.gridx = 4;
																												gbc_button.gridy = 3;
																												panel.add(button, gbc_button);
																												
																												lblNewLabel = new JLabel("");
																												GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
																												gbc_lblNewLabel.insets = new Insets(0, 0, 0, 5);
																												gbc_lblNewLabel.gridx = 1;
																												gbc_lblNewLabel.gridy = 4;
																												panel.add(lblNewLabel, gbc_lblNewLabel);

		panel_1 = new JPanel();
		tabbedPane.addTab("Read Contact", null, panel_1, null);
		panel_1.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);

		panel_2 = new JPanel();
		scrollPane.setViewportView(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));

		textPane = new JTextPane();
		panel_2.add(textPane);
	}

}
