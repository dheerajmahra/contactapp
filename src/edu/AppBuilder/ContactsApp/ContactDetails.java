package edu.AppBuilder.ContactsApp;

import java.io.Serializable;

public class ContactDetails implements Serializable {

	private String name;
	private String email;
	private long phoneNo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) throws Exception {
		if(phoneNo>19999999&&phoneNo<=9999999999l)
		this.phoneNo = phoneNo;
		else
			throw new Exception("Invalid phone no.");
	}
	
}
