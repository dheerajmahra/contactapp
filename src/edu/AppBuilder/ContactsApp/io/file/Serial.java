/**
 * 
 */
package edu.AppBuilder.ContactsApp.io.file;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import edu.AppBuilder.ContactsApp.ContactDetails;
import edu.AppBuilder.ContactsApp.io.IInputOutput;

/**
 * @author khyati
 *
 */

public class Serial implements IInputOutput {


ObjectOutputStream out;
ObjectInputStream in;
	
	/**
	 * 
	 */
	public Serial() {
		
	}

	/* (non-Javadoc)
	 * @see edu.AppBuilder.ContactsApp.io.IInputOutput#createConnection(java.lang.String)
	 */
	@Override
	public void createConnection(String s) throws Exception {
		
		out=new ObjectOutputStream(new FileOutputStream(s,false));
		in=new ObjectInputStream(new FileInputStream(s));

	}

	/* (non-Javadoc)
	 * @see edu.AppBuilder.ContactsApp.io.IInputOutput#save(edu.AppBuilder.ContactsApp.ContactDetails)
	 */
	@Override
	public void save(ContactDetails ref) throws Exception {
		
          out.writeObject(ref);
	}

	/* (non-Javadoc)
	 * @see edu.AppBuilder.ContactsApp.io.IInputOutput#printAll()
	 */
	@Override
	public List<String> printAll() throws Exception {
		
		ContactDetails o;
		ArrayList<String> ar=new ArrayList<String>();
		try{
		while((o=(ContactDetails)in.readObject())!=null)
		{
			
			ar.add(o.getName()+" "+o.getPhoneNo()+" "+o.getEmail());
			
		} }
		
		catch (EOFException e)
		{
			
			
		}
		return ar;
		
	
	}
	/* (non-Javadoc)
	 * @see edu.AppBuilder.ContactsApp.io.IInputOutput#closeConnection()
	 */
	@Override
	public void closeConnection() throws Exception {
		

	}

}
