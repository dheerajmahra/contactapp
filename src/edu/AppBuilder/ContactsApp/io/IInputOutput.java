package edu.AppBuilder.ContactsApp.io;

import java.util.List;

import edu.AppBuilder.ContactsApp.ContactDetails;

public interface IInputOutput {
	
	void createConnection(String s) throws Exception;
	void createConnection(String url,String username,String password) throws Exception;
	
    void save(ContactDetails ref) throws Exception;
    List<String> printAll() throws Exception;
    void closeConnection() throws Exception;
}
